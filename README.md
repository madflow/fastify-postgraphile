# Fastify Plugin for Postgraphile

## Installation

```
yarn add fastify-postgraphile
```

```
npm install fastify-postgraphile
```

## Usage

### Javascript

```JS
const fastify = require('fastify')({
  logger: true
});
const fastifyPostgraphile = require('fastify-postgraphile');

fastify.register(fastifyPostgraphile);

fastify.listen(3000, function(err) {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
});
```

### Typescript / ESM

```JS
import Fastify from 'fastify';
import fastifyPostgraphile from 'fastify-postgraphile';

const fastify = Fastify({
  logger: true
});

fastify.register(fastifyPostgraphile);

fastify.listen(3000, function(err) {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
});
```

## Options

