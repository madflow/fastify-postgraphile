import Fastify from 'fastify';
import fastifyPostgraphile from './src/plugin';
import { RegisterPluginOptions } from './src/interfaces';

const fastify = Fastify({
  logger: true,
});

const options: RegisterPluginOptions = {
  postgraphile: {
    pgConfig: process.env.DATABASE_URL || 'postgresql://',
    graphiql: true,
    enhanceGraphiql: true,
    enableCors: true,
  },
};

fastify.register(fastifyPostgraphile, options);

fastify.listen(3000, function(err) {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
});
