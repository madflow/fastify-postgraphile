"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const fastify_plugin_1 = tslib_1.__importDefault(require("fastify-plugin"));
const pg_1 = require("pg");
const graphql_1 = require("graphql");
const postgraphile_1 = require("postgraphile");
async function plugin(fastify, options, next) {
    const { enhanceGraphiql, graphiql, pgConfig, pgDefaultRole, schemaOptions, schemas, } = options.postgraphile;
    const graphqlRoute = options.postgraphile.graphqlRoute || '/graphql';
    const graphiqlRoute = options.postgraphile.graphiql
        ? options.graphiqlRoute || '/graphiql'
        : null;
    if (graphqlRoute === graphiqlRoute)
        throw new Error(`Cannot use the same route, '${graphqlRoute}', for both GraphQL and GraphiQL. Please use different routes.`);
    let pgPool;
    if (typeof pgConfig === 'string') {
        pgPool = new pg_1.Pool({
            connectionString: pgConfig,
        });
    }
    else {
        throw new Error('Unknown pgConfig option.');
    }
    fastify.addHook('onClose', async (_instance, done) => {
        await pgPool.end();
        done();
    });
    async function performQuery(schema, query, variables, jwtToken, operationName) {
        return await postgraphile_1.withPostGraphileContext({
            pgPool,
            jwtToken: jwtToken,
            jwtSecret: schemaOptions ? schemaOptions.jwtSecret : '',
            pgDefaultRole: pgDefaultRole,
        }, async (context) => {
            return await graphql_1.graphql(schema, query, null, { ...context }, variables, operationName);
        });
    }
    const schema = await postgraphile_1.createPostGraphileSchema(pgPool, schemas || ['public'], schemaOptions);
    const graphqlFastifyRoute = function (fastify, _opts, done) {
        fastify.route({
            method: ['POST'],
            url: graphqlRoute,
            handler: async function (request, reply) {
                const query = request.body.query;
                const variables = request.body.variables || {};
                const res = await performQuery(schema, query, variables, '', '');
                reply.send(res);
            },
        });
        done();
    };
    fastify.register(graphqlFastifyRoute, options);
    if (graphiql) {
        const JS_ESCAPE_LOOKUP = {
            '<': '\\u003c',
            '>': '\\u003e',
            '/': '\\u002f',
            '\u2028': '\\u2028',
            '\u2029': '\\u2029',
        };
        function safeJSONStringify(obj) {
            return JSON.stringify(obj).replace(/[<>/\u2028\u2029]/g, chr => JS_ESCAPE_LOOKUP[chr]);
        }
        const graphiqlHtmlRaw = require('postgraphile/build/assets/graphiql.html')
            .default;
        const graphiqlHtml = graphiqlHtmlRaw
            ? graphiqlHtmlRaw.replace(/<\/head>/, `  <script>window.POSTGRAPHILE_CONFIG=${safeJSONStringify({
                graphqlUrl: `${graphqlRoute}`,
                streamUrl: null,
                enhanceGraphiql,
                subscriptions: null,
                allowExplain: false,
            })};</script>\n  </head>`)
            : null;
        const graphiqlFastifyRoute = function (fastify, _opts, done) {
            fastify.route({
                method: ['GET'],
                url: graphiqlRoute,
                handler: async function (_request, reply) {
                    reply
                        .code(200)
                        .header('Content-Type', 'text/html; charset=utf-8')
                        .send(graphiqlHtml);
                },
            });
            done();
        };
        fastify.register(graphiqlFastifyRoute, options);
    }
    next();
}
exports.default = fastify_plugin_1.default(plugin, {
    fastify: '>=2.0.0',
    name: 'fastify-postgraphile',
});
//# sourceMappingURL=plugin.js.map