/// <reference types="node" />
import { FastifyInstance } from 'fastify';
import { RegisterPluginOptions } from './interfaces';
declare const _default: (instance: FastifyInstance<import("http").Server, import("http").IncomingMessage, import("http").ServerResponse>, options: RegisterPluginOptions, callback: (err?: import("fastify").FastifyError | undefined) => void) => void;
export default _default;
