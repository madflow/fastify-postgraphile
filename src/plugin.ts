import fp from 'fastify-plugin';
import fastifyCors from 'fastify-cors';
import { Pool } from 'pg';
import {
  GraphQLSchema,
  execute as executeGraphql,
  parse as parseGraphql,
  validate as validateGraphql,
  specifiedRules,
  DocumentNode,
} from 'graphql';

import {
  withPostGraphileContext,
  createPostGraphileSchema,
} from 'postgraphile';
import { FastifyInstance } from 'fastify';
import { RegisterPluginOptions } from './interfaces';

async function plugin(
  fastify: FastifyInstance,
  options: RegisterPluginOptions,
  next: Function
) {
  const {
    enhanceGraphiql,
    getGqlSchema,
    graphiql,
    pgConfig,
    pgDefaultRole,
    schemaOptions,
    schemas,
  } = options.postgraphile;

  const graphqlRoute = options.postgraphile.graphqlRoute || '/graphql';
  const graphiqlRoute = options.postgraphile.graphiql
    ? options.postgraphile.graphiqlRoute || '/graphiql'
    : null;

  if (graphqlRoute === graphiqlRoute)
    throw new Error(
      `Cannot use the same route, '${graphqlRoute}', for both GraphQL and GraphiQL. Please use different routes.`
    );

  let pgPool: Pool | any;
  if (typeof pgConfig === 'string') {
    pgPool = new Pool({
      connectionString: pgConfig,
    });
  } else if (typeof pgConfig === 'object') {
    pgPool = pgConfig;
  } else {
    throw new Error('Unknown pgConfig option.');
  }

  fastify.addHook('onClose', async (_instance, done) => {
    await pgPool.end();
    done();
  });

  async function performQuery(
    schema: GraphQLSchema,
    documentAst: DocumentNode,
    variables: Object,
    jwtToken: string,
    operationName: string
  ) {
    return await withPostGraphileContext(
      {
        pgPool,
        jwtToken: jwtToken,
        jwtSecret: schemaOptions ? schemaOptions.jwtSecret : '',
        pgDefaultRole: pgDefaultRole,
      },
      async context => {
        return await executeGraphql(
          schema,
          documentAst,
          null,
          { ...context },
          variables,
          operationName
        );
      }
    );
  }

  const schema = getGqlSchema
    ? await getGqlSchema()
    : await createPostGraphileSchema(
        pgPool,
        schemas || ['public'],
        schemaOptions
      );

  const graphqlFastifyRoute = function(
    fastify: FastifyInstance,
    _opts: any,
    done: Function
  ) {
    fastify.route({
      method: ['POST', 'OPTIONS'],
      url: graphqlRoute,
      handler: async function(request, reply) {
        let documentAst;
        try {
          const query = request.body ? request.body.query : '';
          documentAst = parseGraphql(query);
        } catch (error) {
          reply.status(400).send({
            errors: [error],
          });
        }
        if (documentAst) {
          const validationErrors = validateGraphql(
            schema,
            documentAst,
            specifiedRules
          );

          if (validationErrors.length > 0) {
            reply.status(400).send({ errors: validationErrors });
          } else {
            const variables = (request.body && request.body.variables) ? request.body.variables : {};
            if (variables !== null && typeof variables !== 'object') {
              reply.status(400).send({
                errors: [
                  {
                    message: `Variables must be an object, not '${typeof variables}'.`,
                  },
                ],
              });
            }

            const res = await performQuery(
              schema,
              documentAst,
              variables,
              '',
              ''
            );
            reply.send(res);
          }
        }
      },
    });
    done();
  };

  fastify.register(graphqlFastifyRoute, options);

  if (graphiql && graphiqlRoute) {
    const JS_ESCAPE_LOOKUP = {
      '<': '\\u003c',
      '>': '\\u003e',
      '/': '\\u002f',
      '\u2028': '\\u2028',
      '\u2029': '\\u2029',
    };
    function safeJSONStringify(obj: any) {
      return JSON.stringify(obj).replace(
        /[<>/\u2028\u2029]/g,
        chr => JS_ESCAPE_LOOKUP[chr]
      );
    }
    const graphiqlHtmlRaw = require('postgraphile/build/assets/graphiql.html')
      .default;
    const graphiqlHtml = graphiqlHtmlRaw
      ? graphiqlHtmlRaw.replace(
          /<\/head>/,
          `  <script>window.POSTGRAPHILE_CONFIG=${safeJSONStringify({
            graphqlUrl: `${graphqlRoute}`,
            streamUrl: null,
            enhanceGraphiql,
            subscriptions: null,
            allowExplain: false,
          })};</script>\n  </head>`
        )
      : null;
    const graphiqlFastifyRoute = function(
      fastify: FastifyInstance,
      _opts: any,
      done: Function
    ) {
      fastify.route({
        method: ['GET'],
        url: graphiqlRoute,
        handler: async function(_request, reply) {
          reply
            .code(200)
            .header('Content-Type', 'text/html; charset=utf-8')
            .send(graphiqlHtml);
        },
      });
      done();
    };
    fastify.register(graphiqlFastifyRoute, options);
  }

  // CORS
  if (options.postgraphile.enableCors === true) {
    fastify.register(fastifyCors, {
      methods: ['HEAD', 'GET', 'POST'],
      allowedHeaders: ['Accept, Authorization, X-Apollo-Tracing'],
      exposedHeaders: ['X-GraphQL-Event-Stream'],
    });
  }

  next();
}

export default fp(plugin, {
  fastify: '>=2.0.0',
  name: 'fastify-postgraphile',
});
