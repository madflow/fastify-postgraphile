import Fastify from 'fastify';
import { RegisterPluginOptions } from '../interfaces';
import fastifyPostgraphile from '../plugin';
import { merge } from 'lodash';
import { GraphQLObjectType, GraphQLSchema, GraphQLString } from 'graphql';
import { $$pgClient } from 'postgraphile/build/postgres/inventory/pgClientFromContext';
import { Pool } from 'pg';

const pgClient = {
  query: jest.fn(() => Promise.resolve()),
  release: jest.fn(),
};

export const pgPool = {
  connect: jest.fn(() => pgClient),
  query: jest.fn(),
  end: jest.fn(() => Promise.resolve()),
};

jest.mock('pg', () => {
  return { Pool: jest.fn(() => pgPool) };
});

const gqlSchema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'Query',
    fields: {
      hello: {
        type: GraphQLString,
        resolve: () => 'world',
      },
      greetings: {
        type: GraphQLString,
        args: {
          name: { type: GraphQLString },
        },
        resolve: (_source, { name }) => `Hello, ${name}!`,
      },
      query: {
        type: GraphQLString,
        resolve: (_source, _args, context) =>
          context[$$pgClient].query('EXECUTE'),
      },
      testError: {
        type: GraphQLString,
        resolve: () => {
          const err: any = new Error('test message');
          err.extensions = { testingExtensions: true };
          err.detail = 'test detail';
          err.hint = 'test hint';
          err.code = '12345';
          err.where = 'In your code somewhere';
          err.file = 'alcchk.c';
          throw err;
        },
      },
    },
  }),
  mutation: new GraphQLObjectType({
    name: 'Mutation',
    fields: {
      hello: {
        type: GraphQLString,
        resolve: () => 'world',
      },
    },
  }),
});

export async function buildFastify(testOptions?: RegisterPluginOptions) {
  const fastify = Fastify({
    /**logger: true*/
  });

  const pgConfig = new Pool();

  const options: RegisterPluginOptions = {
    postgraphile: {
      pgConfig: pgConfig,
      getGqlSchema: () => Promise.resolve(gqlSchema),
      graphiql: true,
    },
  };

  const finalOptions: RegisterPluginOptions = merge(options, testOptions);
  fastify.register(fastifyPostgraphile, finalOptions);

  return fastify;
}
