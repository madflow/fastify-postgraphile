import { buildFastify } from './app';
import { FastifyInstance } from 'fastify';

let fastify: FastifyInstance;

beforeEach(async function() {
  fastify = await buildFastify();
  await fastify.ready();
});

afterEach(async function() {
  await fastify.close();
});

test('get request on the graphiql route', async function() {
  try {
    const response = await fastify.inject({
      method: 'GET',
      url: '/graphiql',
    });
    expect(response.statusCode).toBe(200);
    expect(response.headers['content-type']).toBe('text/html; charset=utf-8');
  } catch (err) {
    throw err;
  }
});

test('get request on disabled graphiql route', async function() {
  const myFastify = await buildFastify({ postgraphile: { graphiql: false } });
  await myFastify.ready();
  try {
    const response = await myFastify.inject({
      method: 'GET',
      url: '/graphiql',
    });
    expect(response.statusCode).toBe(404);
  } catch (err) {
    throw err;
  } finally {
    await myFastify.close();
  }
});

test('post request on the graphql route', async function() {
  try {
    const response = await fastify.inject({
      method: 'POST',
      url: '/graphql',
      payload: {
        query: `{hello}`,
      },
    });
    expect(response.statusCode).toBe(200);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
  } catch (err) {
    throw err;
  }
});

test('prefixing the plugin works with custom routes', async function() {
  const myFastify = await buildFastify({
    prefix: '/testing',
    postgraphile: {
      graphiqlRoute: '/heyoo',
      graphqlRoute: '/aaaaaaa',
    },
  });
  await myFastify.ready();
  try {
    const response = await myFastify.inject({
      method: 'POST',
      url: '/testing/aaaaaaa',
      payload: {
        query: `{hello}`,
      },
    });
    expect(response.statusCode).toBe(200);

    const responseGraphiql = await myFastify.inject({
      method: 'GET',
      url: '/testing/heyoo',
    });
    expect(responseGraphiql.statusCode).toBe(200);

    const responseGraphiql404 = await myFastify.inject({
      method: 'GET',
      url: '/graphiql',
    });
    expect(responseGraphiql404.statusCode).toBe(404);
  } catch (err) {
    throw err;
  } finally {
    await myFastify.close();
  }
});

test('will always respond with CORS to an OPTIONS request when enabled', async () => {
  const myFastify = await buildFastify({ postgraphile: { enableCors: true } });
  await myFastify.ready();
  try {
    const response = await myFastify.inject({
      method: 'OPTIONS',
      url: '/graphql',
    });
    expect(response.headers['access-control-allow-origin']).toBe('*');
    expect(response.headers['access-control-allow-methods']).toBe(
      'HEAD, GET, POST'
    );
    expect(response.headers['access-control-allow-headers']).toMatch(
      /Accept, Authorization, X-Apollo-Tracing/
    );
    expect(response.headers['access-control-expose-headers']).toBe(
      'X-GraphQL-Event-Stream'
    );
  } catch (err) {
    throw err;
  } finally {
    await myFastify.close();
  }
});

test('will always respond with CORS to a POST request when enabled', async () => {
  const myFastify = await buildFastify({ postgraphile: { enableCors: true } });
  await myFastify.ready();
  try {
    const response = await myFastify.inject({
      method: 'POST',
      url: '/graphql',
    });
    expect(response.statusCode).toBe(400);
    expect(response.headers['access-control-allow-origin']).toBe('*');
    expect(response.headers['access-control-expose-headers']).toBe(
      'X-GraphQL-Event-Stream'
    );
  } catch (err) {
    throw err;
  } finally {
    await myFastify.close();
  }
});

test('will error if query parse fails', async () => {
  try {
    const response = await fastify.inject({
      method: 'POST',
      url: '/graphql',
      payload: {
        query: `{`,
      },
    });
    const body = JSON.parse(response.payload);
    expect(response.statusCode).toBe(400);
    expect(body.errors).toEqual([
      {
        message: 'Syntax Error: Expected Name, found <EOF>',
        locations: [{ line: 1, column: 2 }],
      },
    ]);
  } catch (err) {
    throw err;
  }
});

test('will error if validation fails', async () => {
  try {
    const response = await fastify.inject({
      method: 'POST',
      url: '/graphql',
      payload: {
        query: `{notFound}`,
      },
    });
    const body = JSON.parse(response.payload);
    expect(response.statusCode).toBe(400);
    expect(body.errors).toEqual([
      {
        message: 'Cannot query field "notFound" on type "Query".',
        locations: [{ line: 1, column: 2 }],
      },
    ]);
  } catch (err) {
    throw err;
  }
});

test('will allow mutations with POST', async () => {
  try {
    const response = await fastify.inject({
      method: 'POST',
      url: '/graphql',
      payload: {
        query: `mutation {hello}`,
      },
    });
    const body = JSON.parse(response.payload);
    expect(response.statusCode).toBe(200);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(body.data).toEqual({ hello: 'world' });
  } catch (err) {
    throw err;
  }
});

test('will ignore empty string variables', async () => {
  try {
    const response = await fastify.inject({
      method: 'POST',
      url: '/graphql',
      payload: {
        query: `{hello}`,
        variables: '',
      },
    });
    const body = JSON.parse(response.payload);
    expect(body.data).toEqual({ hello: 'world' });
  } catch (err) {
    throw err;
  }
});

test('will error with variables of the incorrect type', async () => {
  try {
    const response = await fastify.inject({
      method: 'POST',
      url: '/graphql',
      payload: {
        query: `{hello}`,
        variables: 2,
      },
    });
    const body = JSON.parse(response.payload);
    expect(response.statusCode).toBe(400);
    expect(body.errors).toEqual([
      { message: "Variables must be an object, not 'number'." },
    ]);
  } catch (err) {
    throw err;
  }
});
