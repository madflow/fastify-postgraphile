import { RegisterOptions } from 'fastify';
import { PostGraphileCoreOptions } from 'postgraphile-core';
import { Pool, PoolClient } from 'pg';
import { GraphQLSchema } from 'graphql';

export declare type PgConfig = Pool | PoolClient | string;

export interface FastifyPostGraphileOptions {
  enhanceGraphiql?: boolean;
  enableCors?: boolean;
  getGqlSchema?: () => Promise<GraphQLSchema>;
  graphiql?: boolean;
  graphqlRoute?: string;
  graphiqlRoute?: string;
  pgConfig?: PgConfig;
  pgDefaultRole?: string;
  schemas?: string | string[];
  schemaOptions?: PostGraphileCoreOptions;
}

export interface RegisterPluginOptions extends RegisterOptions<any, any, any> {
  postgraphile: FastifyPostGraphileOptions;
}
